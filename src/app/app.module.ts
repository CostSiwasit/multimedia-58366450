import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AgmCoreModule } from '@agm/core';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AddDrugPage } from '../pages/add-drug/add-drug';
import { SearchDrugPage } from '../pages/search-drug/search-drug';
import { WeighPage } from '../pages/weigh/weigh';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddDrugPage,
    SearchDrugPage,
    WeighPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddDrugPage,
    SearchDrugPage,
    WeighPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
