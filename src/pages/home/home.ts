import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AddDrugPage } from '../add-drug/add-drug';
import { SearchDrugPage } from '../search-drug/search-drug';
import { WeighPage } from '../weigh/weigh';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  addDrug(){
    this.navCtrl.push(AddDrugPage);
  }

  searchDrug(){
    this.navCtrl.push(SearchDrugPage)
  }

  weighMe(){
    this.navCtrl.push(WeighPage)
  }

}
