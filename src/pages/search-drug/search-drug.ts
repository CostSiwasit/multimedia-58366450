import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

declare var google;
import { Plugins } from '@capacitor/core'
import { HomePage } from '../home/home';
const { Geolocation } = Plugins;

@IonicPage()
@Component({
  selector: 'page-search-drug',
  templateUrl: 'search-drug.html',
})
export class SearchDrugPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  start = 'chicago, il';
  end = 'chicago, il';
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchDrugPage');
    this.initMap();
  }

  initMap() {
    Geolocation.getCurrentPosition().then(data => {
      console.log(data)
      let latLng = { lat: data.coords.latitude, lng: data.coords.longitude }
      let mapOptions = {
        center: latLng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
      }
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      var iconok = {
        url: 'assets/imgs/pin.png',
        scaledSize: new google.maps.Size(30, 30),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(15, 30)
      };
      var marker = new google.maps.Marker({
        map: this.map,
        position: latLng,
        icon: iconok
      });
      this.directionsDisplay.setMap(this.map);

      var service = new google.maps.places.PlacesService(this.map);
      service.nearbySearch({
        location: latLng,
        radius: 1000,
        type: ['pharmacy']
      }, (results, status) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            this.createMarker(results[i], this.map);
            // console.log(results[i])
          }
        }
      });
    }).catch(err => {
      console.log(err)
    })

  }

  createMarker(place, map) {
    var iconok = {
      url: 'assets/imgs/pharmacy.png',
      scaledSize: new google.maps.Size(30, 30),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(15, 30)
    };
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
      map: this.map,
      position: placeLoc,
      icon: iconok
    });

    google.maps.event.addListener(marker, 'click', ()=>{
      // this.navCtrl.push(HomePage)
      // window.open(map, marker);
    });
  }

  close() {
    this.navCtrl.pop();
  }


}
