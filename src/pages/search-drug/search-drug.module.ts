import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchDrugPage } from './search-drug';

@NgModule({
  declarations: [
    SearchDrugPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchDrugPage),
  ],
})
export class SearchDrugPageModule {}
