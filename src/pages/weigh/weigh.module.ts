import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeighPage } from './weigh';

@NgModule({
  declarations: [
    WeighPage,
  ],
  imports: [
    IonicPageModule.forChild(WeighPage),
  ],
})
export class WeighPageModule {}
