import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the WeighPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-weigh',
  templateUrl: 'weigh.html',
})
export class WeighPage {

  w:number;
  h:number;
  sum:number;
  result:string="อ้วน"
  isShowModel:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeighPage');
  }
  close() {
    this.navCtrl.pop();
  }
  closex(){
    this.isShowModel=false;
    this.result="";
  }

  cal(){
    this.sum = this.w/((this.h/100)*(this.h/100));
    if(this.sum<18.5){
      this.result = "ต่ำกว่าเกณฑ์"
    }else if(this.sum<=22.9){
      this.result = "สมส่วน"
    }else if(this.sum<=24.9){
      this.result = "น้ำหนักเกิน"
    }else if(this.sum<=29.9){
      this.result = "โรกอ้วน"
    }else{
      this.result = "อันตราราย"
    }
    this.isShowModel=true;
  }

}
