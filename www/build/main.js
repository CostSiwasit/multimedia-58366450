webpackJsonp([3],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDrugPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AddDrugPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddDrugPage = /** @class */ (function () {
    function AddDrugPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.slectIcon = 1;
    }
    AddDrugPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddDrugPage');
    };
    AddDrugPage.prototype.selectMyIcon = function (select) {
        this.slectIcon = select;
    };
    AddDrugPage.prototype.cancel = function () {
        this.navCtrl.pop();
    };
    AddDrugPage.prototype.add = function () {
        this.navCtrl.pop();
    };
    AddDrugPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-drug',template:/*ion-inline-start:"/home/moce/Desktop/cos/drugMe/src/pages/add-drug/add-drug.html"*/'<ion-content>\n  <div class="header-me">\n    เพิ่มยา\n  </div>\n  <div class="card">\n    <div class="title">ตั้งชื่อยา</div>\n    <div class="input">\n      <input type="text" placeholder="ชื่อยา">\n    </div>\n    <div class="title">เลือกเวลาแจ้งเตือน</div>\n    <div class="input">\n        <input type="text" placeholder="เลือกเวลา">\n      <!-- <ion-datetime class="time" displayFormat="h:mm a" placeholder="เลือกเวลา"></ion-datetime> -->\n    </div>\n    <div class="title">เลือกไอคอน</div>\n    <div class="list-icon">\n      <img src="assets/imgs/drug1.png" [class]="slectIcon==1?\'active\':\'\'" (click)="selectMyIcon(1)" alt="">\n      <img src="assets/imgs/drug2.png" [class]="slectIcon==2?\'active\':\'\'" (click)="selectMyIcon(2)" alt="">\n      <img src="assets/imgs/drug3.png" [class]="slectIcon==3?\'active\':\'\'" (click)="selectMyIcon(3)" alt="">\n      <img src="assets/imgs/drug4.png" [class]="slectIcon==4?\'active\':\'\'" (click)="selectMyIcon(4)" alt="">\n      <img src="assets/imgs/drug5.png" [class]="slectIcon==5?\'active\':\'\'" (click)="selectMyIcon(5)" alt="">\n      <img src="assets/imgs/drug6.png" [class]="slectIcon==6?\'active\':\'\'" (click)="selectMyIcon(6)" alt="">\n      <img src="assets/imgs/drug7.png" [class]="slectIcon==7?\'active\':\'\'" (click)="selectMyIcon(7)" alt="">\n      <img src="assets/imgs/drug8.png" [class]="slectIcon==8?\'active\':\'\'" (click)="selectMyIcon(8)" alt="">\n    </div>\n    <div class="btn-l">\n      <div class="btn b" (click)="add()">เพิ่มยา</div>\n      <div class="btn" (click)="cancel()">ยกเลิก</div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/home/moce/Desktop/cos/drugMe/src/pages/add-drug/add-drug.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], AddDrugPage);
    return AddDrugPage;
}());

//# sourceMappingURL=add-drug.js.map

/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchDrugPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__capacitor_core__ = __webpack_require__(250);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Geolocation = __WEBPACK_IMPORTED_MODULE_2__capacitor_core__["a" /* Plugins */].Geolocation;
var SearchDrugPage = /** @class */ (function () {
    function SearchDrugPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.start = 'chicago, il';
        this.end = 'chicago, il';
        this.directionsService = new google.maps.DirectionsService;
        this.directionsDisplay = new google.maps.DirectionsRenderer;
    }
    SearchDrugPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchDrugPage');
        this.initMap();
    };
    SearchDrugPage.prototype.initMap = function () {
        var _this = this;
        Geolocation.getCurrentPosition().then(function (data) {
            console.log(data);
            var latLng = { lat: data.coords.latitude, lng: data.coords.longitude };
            var mapOptions = {
                center: latLng,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
            };
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
            var iconok = {
                url: 'assets/imgs/pin.png',
                scaledSize: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(15, 30)
            };
            var marker = new google.maps.Marker({
                map: _this.map,
                position: latLng,
                icon: iconok
            });
            _this.directionsDisplay.setMap(_this.map);
            var service = new google.maps.places.PlacesService(_this.map);
            service.nearbySearch({
                location: latLng,
                radius: 1000,
                type: ['pharmacy']
            }, function (results, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        _this.createMarker(results[i], _this.map);
                        // console.log(results[i])
                    }
                }
            });
        }).catch(function (err) {
            console.log(err);
        });
    };
    SearchDrugPage.prototype.createMarker = function (place, map) {
        var iconok = {
            url: 'assets/imgs/pharmacy.png',
            scaledSize: new google.maps.Size(30, 30),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(15, 30)
        };
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
            map: this.map,
            position: placeLoc,
            icon: iconok
        });
        google.maps.event.addListener(marker, 'click', function () {
            // this.navCtrl.push(HomePage)
            // window.open(map, marker);
        });
    };
    SearchDrugPage.prototype.close = function () {
        this.navCtrl.pop();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], SearchDrugPage.prototype, "mapElement", void 0);
    SearchDrugPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search-drug',template:/*ion-inline-start:"/home/moce/Desktop/cos/drugMe/src/pages/search-drug/search-drug.html"*/'<ion-content>\n    <div class="header-me">\n      ร้านยาใกล้ตัว\n    </div>\n\n    <div class="header-close" (click)="close()">\n      <img src="assets/imgs/cancel-music.png" alt="">\n    </div>\n\n    <div #map id="map"></div>\n\n  </ion-content>\n  '/*ion-inline-end:"/home/moce/Desktop/cos/drugMe/src/pages/search-drug/search-drug.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], SearchDrugPage);
    return SearchDrugPage;
}());

//# sourceMappingURL=search-drug.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeighPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the WeighPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WeighPage = /** @class */ (function () {
    function WeighPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.result = "อ้วน";
        this.isShowModel = false;
    }
    WeighPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WeighPage');
    };
    WeighPage.prototype.close = function () {
        this.navCtrl.pop();
    };
    WeighPage.prototype.closex = function () {
        this.isShowModel = false;
        this.result = "";
    };
    WeighPage.prototype.cal = function () {
        this.sum = this.w / ((this.h / 100) * (this.h / 100));
        if (this.sum < 18.5) {
            this.result = "ต่ำกว่าเกณฑ์";
        }
        else if (this.sum <= 22.9) {
            this.result = "สมส่วน";
        }
        else if (this.sum <= 24.9) {
            this.result = "น้ำหนักเกิน";
        }
        else if (this.sum <= 29.9) {
            this.result = "โรกอ้วน";
        }
        else {
            this.result = "อันตราราย";
        }
        this.isShowModel = true;
    };
    WeighPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-weigh',template:/*ion-inline-start:"/home/moce/Desktop/cos/drugMe/src/pages/weigh/weigh.html"*/'<ion-content class="content">\n  <div class="header-me">\n      BMI\n  </div>\n\n  <div class="header-close" (click)="close()">\n    <img src="assets/imgs/cancel-music.png" alt="">\n  </div>\n  <div class="close" (click)="close()">\n    <img src="assets/imgs/cancel-music.png" alt="">\n  </div>\n  <div class="item-input">\n    <div class="icon">\n      <div class="image">\n        <img src="assets/imgs/weight-scale.png" alt="">\n      </div>\n      <div class="buble"></div>\n      <div class="buble b1"></div>\n      <div class="buble b2"></div>\n    </div>\n    <div class="input">\n      <input type="number" placeholder="น้ำหนัก (กก.)" [(ngModel)]="w">\n    </div>\n  </div>\n  <div class="item-input">\n    <div class="icon">\n      <div class="image">\n        <img src="assets/imgs/height.png" alt="">\n      </div>\n      <div class="buble"></div>\n      <div class="buble b1"></div>\n      <div class="buble b2"></div>\n    </div>\n    <div class="input">\n      <input type="number" placeholder="ส่วนสูง (ซม.)" [(ngModel)]="h">\n    </div>\n  </div>\n  <div class="input-cal" (click)="cal()">\n    คำนวน\n  </div>\n  <div class="model" *ngIf="isShowModel">\n    \n    <div class="center">\n      <div>{{result}}</div>\n      <div class="close-x" (click)="closex()">\n          ปิด\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/home/moce/Desktop/cos/drugMe/src/pages/weigh/weigh.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], WeighPage);
    return WeighPage;
}());

//# sourceMappingURL=weigh.js.map

/***/ }),

/***/ 112:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 112;

/***/ }),

/***/ 153:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-drug/add-drug.module": [
		289,
		2
	],
	"../pages/search-drug/search-drug.module": [
		290,
		1
	],
	"../pages/weigh/weigh.module": [
		291,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 153;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_drug_add_drug__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_drug_search_drug__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__weigh_weigh__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage.prototype.addDrug = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__add_drug_add_drug__["a" /* AddDrugPage */]);
    };
    HomePage.prototype.searchDrug = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__search_drug_search_drug__["a" /* SearchDrugPage */]);
    };
    HomePage.prototype.weighMe = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__weigh_weigh__["a" /* WeighPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/moce/Desktop/cos/drugMe/src/pages/home/home.html"*/'<ion-content class="content-bg" fullscreen>\n  <div class="header-me">\n    <div class="ani-circle">\n      <div class="shadow-box"></div>\n      <div class="circle c1"></div>\n      <div class="circle c2"></div>\n      <div class="circle c3"></div>\n      <div class="circle c4"></div>\n      <div class="circle c5"></div>\n      <div class="text">\n        <div class="text1">10</div>\n        <div class="text2">นาที</div>\n      </div>\n    </div>\n  </div>\n  <div>\n    <div class="item-drug">\n      <div class="icon">\n        <img src="assets/imgs/drug1.png" alt="">\n      </div>\n      <div class="label">ยาพาลา</div>\n    </div>\n    <div class="item-drug">\n      <div class="icon">\n        <img src="assets/imgs/drug2.png" alt="">\n      </div>\n      <div class="label">ยาแก้ไอ</div>\n    </div>\n  </div>\n  <div class="more-box">\n    <div class="more-box-circle"></div>\n    <div class="more-box-content">\n      <div class="item-me" (click)="addDrug()">\n        <div class="icon">\n          <img src="assets/imgs/drugs.svg" alt="">\n        </div>\n        <div class="label">เพิ่มยา</div>\n      </div>\n      <div class="item-me" (click)="searchDrug()">\n        <div class="icon">\n          <img src="assets/imgs/magnifier.svg" alt="">\n        </div>\n        <div class="label">ค้นหาร้าน</div>\n      </div>\n      <div class="item-me" (click)="weighMe()">\n        <div class="icon">\n          <img src="assets/imgs/dumbbell.svg" alt="">\n        </div>\n        <div class="label">วิเคราะน้ำหนัก</div>\n      </div>\n    </div>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/home/moce/Desktop/cos/drugMe/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(224);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_add_drug_add_drug__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_search_drug_search_drug__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_weigh_weigh__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_add_drug_add_drug__["a" /* AddDrugPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_search_drug_search_drug__["a" /* SearchDrugPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_weigh_weigh__["a" /* WeighPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/add-drug/add-drug.module#AddDrugPageModule', name: 'AddDrugPage', segment: 'add-drug', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-drug/search-drug.module#SearchDrugPageModule', name: 'SearchDrugPage', segment: 'search-drug', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/weigh/weigh.module#WeighPageModule', name: 'WeighPage', segment: 'weigh', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_add_drug_add_drug__["a" /* AddDrugPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_search_drug_search_drug__["a" /* SearchDrugPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_weigh_weigh__["a" /* WeighPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/moce/Desktop/cos/drugMe/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/moce/Desktop/cos/drugMe/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[201]);
//# sourceMappingURL=main.js.map